package com.openclassrooms.parent.app.rss.feeds.service;

import com.openclassrooms.parent.app.rss.feeds.dto.FeedItemDto;
import com.openclassrooms.parent.app.rss.feeds.dto.ImageDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class RssParserServiceTest {

    @InjectMocks
    private RssParserService rssParserService;

    @Test
    void readFeed() {
        // GIVEN
        FeedItemDto feedItemDto1 = FeedItemDto.builder()
                .title("Titre 1")
                .url("https://www.parentApp.fr/actualites/article1")
                .description("Description article 1")
                .image(ImageDto.builder().type("image/jpg").url("https://i.unimedias.fr/2021/05/07/vaccincovidenfant.jpg?auto=format%2Ccompress&crop=faces&cs=tinysrgb&fit=max&w=1500#xtor=RSS-59").build())
                .build();
        FeedItemDto feedItemDto2 = FeedItemDto.builder()
                .title("Titre 2")
                .url("https://www.parentApp.fr/actualites/article2")
                .description("Description article 2 a > b Jack & Jones")
                .image(ImageDto.builder().type("image/jpg").url("https://i.unimedias.fr/2021/05/07/vaccincovidenfant.jpg?auto=format%2Ccompress&crop=faces&cs=tinysrgb&fit=max&w=1500#xtor=RSS-59").build())
                .build();

        // WHEN
        List<FeedItemDto> results = rssParserService.readFeed("file:src/test/resources/valid-rss-feed.xml");

        // THEN
        Assertions.assertThat(results).containsExactlyInAnyOrder(feedItemDto1, feedItemDto2);
    }
}
