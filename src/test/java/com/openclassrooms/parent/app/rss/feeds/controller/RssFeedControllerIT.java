package com.openclassrooms.parent.app.rss.feeds.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.parent.app.rss.feeds.dto.RssFeedDto;
import com.openclassrooms.parent.app.rss.feeds.dto.SavedFeedItemDto;
import com.openclassrooms.parent.app.rss.feeds.entity.RssFeed;
import com.openclassrooms.parent.app.rss.feeds.entity.SavedFeedItem;
import com.openclassrooms.parent.app.rss.feeds.entity.UserRssFeed;
import com.openclassrooms.parent.app.rss.feeds.repository.RssFeedRespository;
import com.openclassrooms.parent.app.rss.feeds.repository.SavedFeedItemRepository;
import com.openclassrooms.parent.app.rss.feeds.repository.UserRssFeedRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class RssFeedControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRssFeedRepository userRssFeedRepository;

    @Autowired
    private RssFeedRespository rssFeedRespository;

    @Autowired
    private SavedFeedItemRepository savedFeedItemRepository;

    @Autowired
    private ObjectMapper mapper;

    @AfterEach
    public void clearData() {
        userRssFeedRepository.deleteAll();
        savedFeedItemRepository.deleteAll();
    }

    @Test
    void getAllSources() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(RssFeedsController.ROOT_RESOURCE))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(9)))
                .andReturn();
    }

    @Test
    void getAllSources_withUserId() throws Exception {
        // GIVEN
        UserRssFeed userRssFeed = UserRssFeed.builder().title("title").userId("userId").url("url").build();
        userRssFeedRepository.save(userRssFeed);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(RssFeedsController.ROOT_RESOURCE)
                .param("userId", "userId"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(10)))
                .andReturn();

    }

    @Test
    void getFeedItems() throws Exception {
        // GIVEN
        RssFeed rssFeed = new RssFeed();
        rssFeed.setTitle("titre");
        rssFeed.setUrl("file:src/test/resources/valid-rss-feed.xml");
        RssFeed entitySaved = rssFeedRespository.save(rssFeed);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(RssFeedsController.ROOT_RESOURCE + "/"  + entitySaved.getId() + "/news"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn();

        rssFeedRespository.delete(entitySaved);
    }

    @Test
    void getAllUserFeedItems() throws Exception {
        // GIVEN
        SavedFeedItem savedFeedItem1 = SavedFeedItem.builder().userId("userId").url("url1").title("title1").build();
        SavedFeedItem savedFeedItem2 = SavedFeedItem.builder().userId("userId").url("url2").title("title2").build();
        SavedFeedItem savedFeedItem3 = SavedFeedItem.builder().userId("userId2").url("url3").title("title3").build();
        savedFeedItemRepository.saveAll(List.of(savedFeedItem1, savedFeedItem2, savedFeedItem3));

        // WHEN
         mockMvc.perform(MockMvcRequestBuilders.get(RssFeedsController.ROOT_RESOURCE + RssFeedsController.SAVED_FEED_ITEMS_RESOURCE)
                .param("userId", "userId"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andReturn();
    }

    @Test
    void saveFeedItem() throws Exception {
        // GIVEN
        SavedFeedItemDto savedFeedItemDto = SavedFeedItemDto.builder().url("url").userId("userId").title("title").build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(RssFeedsController.ROOT_RESOURCE + RssFeedsController.SAVED_FEED_ITEMS_RESOURCE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(savedFeedItemDto)))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();
    }

    @Test
    void deleteExistingSavedFeedItem() throws Exception {
        // GIVEN
        SavedFeedItem savedFeedItem = SavedFeedItem.builder().userId("userId").url("url1").title("title1").build();
        SavedFeedItem entitySaved = savedFeedItemRepository.save(savedFeedItem);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.delete(RssFeedsController.ROOT_RESOURCE + RssFeedsController.SAVED_FEED_ITEMS_RESOURCE + "/" + entitySaved.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Assertions.assertThat(savedFeedItemRepository.findById(entitySaved.getId())).isNotPresent();
    }

    @Test
    void deleteNonExistingSavedFeedItem() throws Exception {
        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.delete(RssFeedsController.ROOT_RESOURCE + RssFeedsController.SAVED_FEED_ITEMS_RESOURCE + "/4554578785454"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void getAllUserRssFeeds() throws Exception {
        // GIVEN
        UserRssFeed userRssFeed1 = UserRssFeed.builder().userId("userId").url("url1").title("title1").build();
        UserRssFeed userRssFeed2 = UserRssFeed.builder().userId("userId").url("url2").title("title2").build();
        UserRssFeed userRssFeed3 = UserRssFeed.builder().userId("userId2").url("url3").title("title3").build();
        userRssFeedRepository.saveAll(List.of(userRssFeed1, userRssFeed2, userRssFeed3));

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.get(RssFeedsController.ROOT_RESOURCE + RssFeedsController.USER_FEEDS_RESOURCE)
                .param("userId", "userId"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn();
    }

    @Test
    void createUserRssFeedValidationSucceed() throws Exception {
        // GIVEN
        RssFeedDto rssFeedDto = RssFeedDto.builder()
                .userId("userId")
                .title("title")
                .url("file:src/test/resources/valid-rss-feed.xml")
                .build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(RssFeedsController.ROOT_RESOURCE + RssFeedsController.USER_FEEDS_RESOURCE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(rssFeedDto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.message", is("Flux rss ajouté")))
                .andReturn();
    }

    @Test
    void createUserRssFeedValidationFailed() throws Exception {
        // GIVEN
        RssFeedDto rssFeedDto = RssFeedDto.builder()
                .userId("userId")
                .title("title")
                .url("file:src/test/resources/invalid-rss-feed.xml")
                .build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.post(RssFeedsController.ROOT_RESOURCE + RssFeedsController.USER_FEEDS_RESOURCE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(rssFeedDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.message", is("La création du flux rss a échoué : Flux rss invalide")))
                .andReturn();
    }

    @Test
    void updateUserRssFeedSucceed() throws Exception {
        // GIVEN
        UserRssFeed userRssFeed = UserRssFeed.builder()
                .userId("userId")
                .url("file:src/test/resources/valid-rss-feed.xml")
                .title("title")
                .build();
        UserRssFeed entitySaved = userRssFeedRepository.save(userRssFeed);

        RssFeedDto rssFeedDto = RssFeedDto.builder()
                .id(entitySaved.getId())
                .userId("userId")
                .url("file:src/test/resources/valid-rss-feed.xml")
                .title("title updated")
                .build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(RssFeedsController.ROOT_RESOURCE + RssFeedsController.USER_FEEDS_RESOURCE + "/" + entitySaved.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(rssFeedDto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.message", is("Flux rss mis à jour")));
    }

    @Test
    void updateUserRssFeedFailed() throws Exception {
        // GIVEN
        UserRssFeed userRssFeed = UserRssFeed.builder()
                .userId("userId")
                .url("file:src/test/resources/valid-rss-feed.xml")
                .title("title")
                .build();
        UserRssFeed entitySaved = userRssFeedRepository.save(userRssFeed);

        RssFeedDto rssFeedDto = RssFeedDto.builder()
                .id(entitySaved.getId())
                .userId("userId")
                .url("file:src/test/resources/invalid-rss-feed.xml")
                .title("title")
                .build();

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.put(RssFeedsController.ROOT_RESOURCE + RssFeedsController.USER_FEEDS_RESOURCE + "/" + entitySaved.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(rssFeedDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.message", is("La mise à jour du flux rss a échouée : Flux rss invalide")));
    }

    @Test
    void deleteUserRssFeed() throws Exception {
        // GIVEN
        UserRssFeed userRssFeed = UserRssFeed.builder()
                .userId("userId")
                .url("file:src/test/resources/valid-rss-feed.xml")
                .title("title")
                .build();
        UserRssFeed entitySaved = userRssFeedRepository.save(userRssFeed);

        // WHEN
        mockMvc.perform(MockMvcRequestBuilders.delete(RssFeedsController.ROOT_RESOURCE + RssFeedsController.USER_FEEDS_RESOURCE + "/" + entitySaved.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Assertions.assertThat(userRssFeedRepository.findById(entitySaved.getId())).isNotPresent();
    }


}
