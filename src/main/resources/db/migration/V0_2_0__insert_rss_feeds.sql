INSERT INTO t_rss_feed(url, title)
VALUES
('https://www.parents.fr/feeds/rss/actualites', 'Parents.fr : toutes les actualités'),
('https://www.parents.fr/feeds/rss/envie-de-bebe', 'Parents.fr : derniers articles univers Envie de bébé'),
('https://www.parents.fr/feeds/rss/grossesse', 'Parents.fr : derniers articles univers Grossesse'),
('https://www.parents.fr/feeds/rss/accouchement', 'Parents.fr : derniers articles univers Accouchement'),
('https://www.parents.fr/feeds/rss/bebe', 'Parents.fr : derniers articles univers Bébé'),
('https://www.parents.fr/feeds/rss/enfant', 'Parents.fr : derniers articles univers Enfant'),
('https://www.parents.fr/feeds/rss/etre-parent', 'Parents.fr : derniers articles univers Parents'),
('https://www.parents.fr/feeds/rss/prenoms', 'Parents.fr : derniers articles univers Prénoms'),
('https://www.parents.fr/feeds/rss/bien-s-equiper', 'Parents.fr : derniers articles univers Bien s''équiper');
