CREATE SCHEMA IF NOT EXISTS rss_feeds;

CREATE TABLE t_rss_feed
(
    id BIGSERIAL PRIMARY KEY,
    url TEXT NOT NULL UNIQUE,
    title VARCHAR(255) NOT NULL
);

CREATE TABLE t_saved_feed_item
(
    id BIGSERIAL PRIMARY KEY,
    user_id VARCHAR(250) NOT NULL,
    url TEXT NOT NULL,
    title TEXT NOT NULL,
    creation_date DATE NOT NULL
);

CREATE TABLE t_user_rss_feed
(
    id BIGSERIAL PRIMARY KEY,
    url TEXT NOT NULL,
    title VARCHAR(255) NOT NULL,
    user_id VARCHAR(250) NOT NULL
);
