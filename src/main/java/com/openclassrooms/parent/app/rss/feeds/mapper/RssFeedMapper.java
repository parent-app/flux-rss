package com.openclassrooms.parent.app.rss.feeds.mapper;

import com.openclassrooms.parent.app.rss.feeds.dto.RssFeedDto;
import com.openclassrooms.parent.app.rss.feeds.entity.RssFeed;
import com.openclassrooms.parent.app.rss.feeds.entity.UserRssFeed;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RssFeedMapper {

    @Mapping(target = "userId", ignore = true)
    List<RssFeedDto> rssFeedToListDto(Iterable<RssFeed> rssFeeds);

    List<RssFeedDto> userRssFeedToListDto(Iterable<UserRssFeed> userRssFeeds);

    UserRssFeed toUserRssFeed(RssFeedDto dto);

    RssFeedDto userRssFeedToDto(UserRssFeed userRssFeed);
}
