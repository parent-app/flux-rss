package com.openclassrooms.parent.app.rss.feeds.service;

import com.openclassrooms.parent.app.rss.feeds.dto.SavedFeedItemDto;
import com.openclassrooms.parent.app.rss.feeds.entity.SavedFeedItem;
import com.openclassrooms.parent.app.rss.feeds.exception.AppEntityNotFoundException;
import com.openclassrooms.parent.app.rss.feeds.exception.FeedItemAlreadySavedException;
import com.openclassrooms.parent.app.rss.feeds.mapper.SavedFeedItemMapper;
import com.openclassrooms.parent.app.rss.feeds.repository.SavedFeedItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SavedFeedItemService {

    private final SavedFeedItemRepository savedFeedItemRepository;
    private final SavedFeedItemMapper mapper;

    public Page<SavedFeedItemDto> getAllByUserId(String userId, Pageable page) {
        return savedFeedItemRepository.findAllByUserId(userId, page).map(mapper::toSavedFeedItemDto);
    }

    public void createFeedItemSaved(SavedFeedItemDto feedItemSaved) {
        checkIfItemIsAlreadySaved(feedItemSaved);
        savedFeedItemRepository.save(mapper.toFeedItemSaved(feedItemSaved));
    }

    public void deleteFeedItemSaved(Long id) {
        SavedFeedItem entity = this.savedFeedItemRepository.findById(id).orElseThrow(AppEntityNotFoundException::new);
        savedFeedItemRepository.delete(entity);
    }

    private void checkIfItemIsAlreadySaved(SavedFeedItemDto feedItem) throws FeedItemAlreadySavedException{
        savedFeedItemRepository.findByUserIdAndUrl(feedItem.getUserId(), feedItem.getUrl()).ifPresent(item -> {
            throw new FeedItemAlreadySavedException();
        });
    }
}
