package com.openclassrooms.parent.app.rss.feeds.repository;

import com.openclassrooms.parent.app.rss.feeds.entity.UserRssFeed;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRssFeedRepository extends CrudRepository<UserRssFeed, Long> {

    Iterable<UserRssFeed> findAllByUserId(String userId);
    Optional<UserRssFeed> findByIdAndUserId(Long id, String userId);
}
