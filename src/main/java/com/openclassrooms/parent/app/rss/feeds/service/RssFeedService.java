package com.openclassrooms.parent.app.rss.feeds.service;

import com.openclassrooms.parent.app.rss.feeds.dto.RssFeedDto;
import com.openclassrooms.parent.app.rss.feeds.entity.RssFeed;
import com.openclassrooms.parent.app.rss.feeds.exception.AppEntityNotFoundException;
import com.openclassrooms.parent.app.rss.feeds.mapper.RssFeedMapper;
import com.openclassrooms.parent.app.rss.feeds.repository.RssFeedRespository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RssFeedService {

    private final RssFeedRespository rssFeedRespository;
    private final RssFeedMapper mapper;

    public List<RssFeedDto> findAll() {
        return mapper.rssFeedToListDto(rssFeedRespository.findAll());
    }

    public RssFeed findById(Long id) {
        return rssFeedRespository.findById(id).orElseThrow(AppEntityNotFoundException::new);
    }
}
