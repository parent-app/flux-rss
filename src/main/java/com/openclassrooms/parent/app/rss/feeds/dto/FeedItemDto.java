package com.openclassrooms.parent.app.rss.feeds.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FeedItemDto {

    private String title;
    private String url;
    private String description;
    private ImageDto image;
}
