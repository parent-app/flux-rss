package com.openclassrooms.parent.app.rss.feeds.controller;

import com.openclassrooms.parent.app.rss.feeds.dto.FeedItemDto;
import com.openclassrooms.parent.app.rss.feeds.dto.RssFeedDto;
import com.openclassrooms.parent.app.rss.feeds.dto.SavedFeedItemDto;
import com.openclassrooms.parent.app.rss.feeds.entity.RssFeed;
import com.openclassrooms.parent.app.rss.feeds.entity.UserRssFeed;
import com.openclassrooms.parent.app.rss.feeds.exception.AppEntityNotFoundException;
import com.openclassrooms.parent.app.rss.feeds.model.ResponseMessage;
import com.openclassrooms.parent.app.rss.feeds.model.RssMessage;
import com.openclassrooms.parent.app.rss.feeds.service.SavedFeedItemService;
import com.openclassrooms.parent.app.rss.feeds.service.RssParserService;
import com.openclassrooms.parent.app.rss.feeds.service.RssFeedService;
import com.openclassrooms.parent.app.rss.feeds.service.UserRssFeedService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping(RssFeedsController.ROOT_RESOURCE)
@Tag(name = "rssFeeds", description = "Handler for rss feeds requests")
public class RssFeedsController {

    public static final String ROOT_RESOURCE = "/api/feeds";
    public static final String USER_FEEDS_RESOURCE = "/users";
    public static final String SAVED_FEED_ITEMS_RESOURCE = "/saved";

    private final RssFeedService rssFeedService;
    private final SavedFeedItemService savedFeedItemService;
    private final RssParserService rssParserService;
    private final UserRssFeedService userRssFeedService;

    @GetMapping
    @Operation(summary = "Get all rss feed sources")
    public List<RssFeedDto> getAllSources(@Parameter(name = "userId", description = "User id") @RequestParam(required = false) String userId) {
        List<RssFeedDto> results = rssFeedService.findAll();
        if (userId != null) {
            results.addAll(userRssFeedService.findAllByUserId(userId));
        }
        return results;
    }

    @GetMapping("/{id}/news")
    @Operation(summary = "Get all news from a rss feed")
    public List<FeedItemDto> getFeedItems(@Parameter(name = "id", description = "Rss feed id") @PathVariable Long id,
                                          @Parameter(name = "userId", description = "Id of the user") @RequestParam(required = false) String userId) {
        try {
            RssFeed rssFeed = rssFeedService.findById(id);
            return rssParserService.readFeed(rssFeed.getUrl());
        } catch (AppEntityNotFoundException e) {
            RssFeedDto userRssFeed = userRssFeedService.findById(id, userId);
            return rssParserService.readFeed(userRssFeed.getUrl());
        }
    }

    @GetMapping(SAVED_FEED_ITEMS_RESOURCE)
    @Operation(summary = "Get all user'saved feed items")
    public Page<SavedFeedItemDto> getAllUserFeedItems(@Parameter(name = "userId", description = "User id") @RequestParam String userId,
                                                      @Parameter(name = "page", description = "Page number to display. Default value : 0") @RequestParam(defaultValue = "0") Integer page,
                                                      @Parameter(name = "size", description = "Page size. Default value : 10") @RequestParam(defaultValue = "10") Integer size,
                                                      @Parameter(name = "direction", description = "Sort direction. Default Value : DESC") @RequestParam(defaultValue = "DESC") Sort.Direction direction,
                                                      @Parameter(name = "unpaged", description = "Boolean allowing pagination to be disabled") @RequestParam(defaultValue = "false") boolean unpaged) {
        Pageable pageable = unpaged ? Pageable.unpaged() : PageRequest.of(page, size, direction, "creationDate");
        return savedFeedItemService.getAllByUserId(userId, pageable);
    }

    @PostMapping(SAVED_FEED_ITEMS_RESOURCE)
    @Operation(summary = "Add item to saved items")
    public ResponseEntity<Void> saveFeedItem(@RequestBody SavedFeedItemDto feedItem) {
        savedFeedItemService.createFeedItemSaved(feedItem);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(SAVED_FEED_ITEMS_RESOURCE + "/{id}")
    @Operation(summary = "Delete an item from saved items")
    public ResponseEntity<Void> deleteFeedItem(@Parameter(name = "id", description = "Saved item id") @PathVariable Long id) {
        try {
            savedFeedItemService.deleteFeedItemSaved(id);
            return ResponseEntity.noContent().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(USER_FEEDS_RESOURCE)
    @Operation(summary = "Get all user rss feeds")
    public List<RssFeedDto> getAllUserRssFeeds(@Parameter(name = "userId", description = "User id") @RequestParam String userId) {
        return userRssFeedService.findAllByUserId(userId);
    }

    // TODO : voir si nécessaire
    @GetMapping(USER_FEEDS_RESOURCE + "/{id}")
    @Operation(summary = "Get a user rss fedd by id")
    public RssFeedDto getUserRssFeedById(@Parameter(name = "id", description = "Id of the requested user rss feed") @PathVariable Long id,
                                         @Parameter(name = "userId", description = "Id of the user") @RequestParam String userId) {
        return userRssFeedService.findById(id, userId);
    }

    @PostMapping(USER_FEEDS_RESOURCE)
    @Operation(summary = "add a rss feed")
    public ResponseEntity<ResponseMessage> createUserRssFeed(@RequestBody RssFeedDto rssFeedDto) {
        RssMessage message = userRssFeedService.create(rssFeedDto);
        if (message == RssMessage.VALID) {
            return ResponseEntity.ok(new ResponseMessage("Flux rss ajouté"));
        }
        return ResponseEntity.badRequest().body(new ResponseMessage("La création du flux rss a échoué : " + message.getMessage()));
    }

    @PutMapping(value = USER_FEEDS_RESOURCE + "/{id}")
    @Operation(summary =  "Update user rss feed")
    public ResponseEntity<ResponseMessage> updateUserRssFeed(@Parameter(name = "id", description = "Id of the user rss feed") @PathVariable Long id,
                                                             @RequestBody RssFeedDto rssFeedDto) {
        RssMessage message = userRssFeedService.update(rssFeedDto);
        if (message == RssMessage.VALID) {
            return ResponseEntity.ok(new ResponseMessage("Flux rss mis à jour"));
        }
        return ResponseEntity.badRequest().body(new ResponseMessage("La mise à jour du flux rss a échouée : " + message.getMessage()));
    }

    @DeleteMapping(USER_FEEDS_RESOURCE + "/{id}")
    @Operation(summary = "Delete user rss feed")
    public ResponseEntity<Void> deleteUserRssFeed(@Parameter(name = "id", description = "Id of the user rss feed") @PathVariable Long id) {
        userRssFeedService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
