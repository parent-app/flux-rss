package com.openclassrooms.parent.app.rss.feeds.mapper;

import com.openclassrooms.parent.app.rss.feeds.dto.SavedFeedItemDto;
import com.openclassrooms.parent.app.rss.feeds.entity.SavedFeedItem;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SavedFeedItemMapper {

    List<SavedFeedItemDto> toListFeedSavedDto(Iterable<SavedFeedItem> feedsSaved);
    SavedFeedItemDto toSavedFeedItemDto(SavedFeedItem feedItem);
    SavedFeedItem toFeedItemSaved(SavedFeedItemDto dto);
}
