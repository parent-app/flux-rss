package com.openclassrooms.parent.app.rss.feeds.model;

public enum RssMessage {
    VALID("Flux rss valide"),
    INVALID("Flux rss invalide");

    private final String message;

    RssMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
