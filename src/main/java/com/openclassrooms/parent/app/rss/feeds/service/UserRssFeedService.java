package com.openclassrooms.parent.app.rss.feeds.service;

import com.openclassrooms.parent.app.rss.feeds.dto.RssFeedDto;
import com.openclassrooms.parent.app.rss.feeds.entity.UserRssFeed;
import com.openclassrooms.parent.app.rss.feeds.exception.AppEntityNotFoundException;
import com.openclassrooms.parent.app.rss.feeds.mapper.RssFeedMapper;
import com.openclassrooms.parent.app.rss.feeds.model.RssMessage;
import com.openclassrooms.parent.app.rss.feeds.repository.UserRssFeedRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserRssFeedService {

    private final UserRssFeedRepository userRssFeedRepository;
    private final RssFeedMapper mapper;
    private final RssParserService rssParserService;

    public List<RssFeedDto> findAllByUserId(String userId) {
        return mapper.userRssFeedToListDto(userRssFeedRepository.findAllByUserId(userId));
    }

    public RssFeedDto findById(Long id, String userId) {
        return mapper.userRssFeedToDto(userRssFeedRepository.findByIdAndUserId(id, userId).orElseThrow(AppEntityNotFoundException::new));
    }

    public RssMessage create(RssFeedDto rssFeedDto) {
        if (rssParserService.validateFeed(rssFeedDto.getUrl())) {
            userRssFeedRepository.save(mapper.toUserRssFeed(rssFeedDto));
            return RssMessage.VALID;
        }
        return RssMessage.INVALID;
    }

    public RssMessage update(RssFeedDto rssFeedDto) {
        if (rssParserService.validateFeed(rssFeedDto.getUrl())) {
            UserRssFeed entity = userRssFeedRepository.findById(rssFeedDto.getId()).orElseThrow(AppEntityNotFoundException::new);
            entity.setTitle(rssFeedDto.getTitle());
            entity.setUrl(rssFeedDto.getUrl());
            userRssFeedRepository.save(entity);
            return RssMessage.VALID;
        }
        return RssMessage.INVALID;
    }

    public void delete(Long id) {
        UserRssFeed entity = userRssFeedRepository.findById(id).orElseThrow(AppEntityNotFoundException::new);
        userRssFeedRepository.delete(entity);
    }
}
