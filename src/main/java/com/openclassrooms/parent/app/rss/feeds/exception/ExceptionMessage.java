package com.openclassrooms.parent.app.rss.feeds.exception;

public final class ExceptionMessage {

    private ExceptionMessage() {}

    public static final String RSS_PARSER_ERROR = "Erreur lors de la lecture du flux RSS : veuillez verifier la validité du flux";
}
