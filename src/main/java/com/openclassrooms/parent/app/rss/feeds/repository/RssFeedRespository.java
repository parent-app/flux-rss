package com.openclassrooms.parent.app.rss.feeds.repository;

import com.openclassrooms.parent.app.rss.feeds.entity.RssFeed;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RssFeedRespository extends CrudRepository<RssFeed, Long> {
}
