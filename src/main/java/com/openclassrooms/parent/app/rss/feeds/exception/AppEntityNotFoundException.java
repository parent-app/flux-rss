package com.openclassrooms.parent.app.rss.feeds.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AppEntityNotFoundException extends ResponseStatusException {

    public static final AppEntityNotFoundException INSTANCE = new AppEntityNotFoundException();

    public AppEntityNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Entité non trouvée");
    }
}
