package com.openclassrooms.parent.app.rss.feeds.service;

import com.openclassrooms.parent.app.rss.feeds.dto.FeedItemDto;
import com.openclassrooms.parent.app.rss.feeds.dto.ImageDto;
import com.openclassrooms.parent.app.rss.feeds.exception.ExceptionMessage;
import com.openclassrooms.parent.app.rss.feeds.exception.FunctionalException;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class RssParserService {

    public List<FeedItemDto> readFeed(String url) {
        SyndFeed feed = parseFeed(url);
        return feed != null ? buildFeedItemsList(feed) : List.of();
    }

    public boolean validateFeed(String url) {
        try {
            parseFeed(url);
            return true;
        } catch (FunctionalException e) {
            return false;
        }
    }

    private SyndFeed parseFeed(String url) {
        try (var reader = new XmlReader(new URL(url))) {
            return new SyndFeedInput().build(reader);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new FunctionalException(ExceptionMessage.RSS_PARSER_ERROR);
        }
    }

    private List<FeedItemDto> buildFeedItemsList(SyndFeed feed)  {
        List<FeedItemDto> results = new ArrayList<>();
        for (SyndEntry entry : feed.getEntries()) {
            var item = new FeedItemDto();
            item.setUrl(entry.getLink());
            item.setDescription(Jsoup.parse(entry.getDescription().getValue()).text());
            item.setTitle(Jsoup.parse(entry.getTitle()).text());
           if (entry.getEnclosures() != null && !entry.getEnclosures().isEmpty()) {
               item.setImage(new ImageDto(entry.getEnclosures().get(0).getType(), entry.getEnclosures().get(0).getUrl()));
           }
           results.add(item);
        }
        return results;
    }
}
