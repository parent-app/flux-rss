package com.openclassrooms.parent.app.rss.feeds.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class FeedItemAlreadySavedException extends ResponseStatusException {

    public static final FeedItemAlreadySavedException INSTANCE = new FeedItemAlreadySavedException();

    public FeedItemAlreadySavedException() {
        super(HttpStatus.BAD_REQUEST, "Article déjà sauvegardé");
    }
}
