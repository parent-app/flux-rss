package com.openclassrooms.parent.app.rss.feeds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParentAppRssFeedsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParentAppRssFeedsApplication.class, args);
	}

}
