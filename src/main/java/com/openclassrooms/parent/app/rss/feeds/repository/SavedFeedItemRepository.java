package com.openclassrooms.parent.app.rss.feeds.repository;

import com.openclassrooms.parent.app.rss.feeds.entity.SavedFeedItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface SavedFeedItemRepository extends PagingAndSortingRepository<SavedFeedItem, Long> {

    Page<SavedFeedItem> findAllByUserId(String userId, Pageable page);
    Optional<SavedFeedItem> findByUserIdAndUrl(String userId, String url);
}
