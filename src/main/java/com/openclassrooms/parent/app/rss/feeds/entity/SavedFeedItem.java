package com.openclassrooms.parent.app.rss.feeds.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "t_saved_feed_item", schema = "rss_feeds")
public class SavedFeedItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    @NotNull
    private String userId;

    @Column(columnDefinition = "TEXT")
    @NotNull
    private String url;

    @Column(columnDefinition = "TEXT")
    @NotNull
    private String title;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @PrePersist
    @PreUpdate
    private void setCreationDate() {
        this.creationDate = LocalDate.now();
    }
}
